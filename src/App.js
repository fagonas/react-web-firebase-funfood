import React, {Component} from 'react';
import './App.css';
import firebase from './firebase.js';

class App extends Component {
    constructor() {
        super();
        this.state = {
            currentItem: '',
            username: '',
            items: []
        };
        this.id = 1;
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.itemsRef = firebase.database().ref('items');
        this.itemsRef.orderByKey().limitToLast(1).once('value', snapshot => {
            snapshot.forEach(item => {
                const lastItem = item.val();
                this.id = lastItem.id + 1;
                console.log("last key is ", this.id);
            });
        })
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        const item = {
            id: this.id++,
            title: this.state.currentItem,
            user: this.state.username,
            time: firebase.database.ServerValue.TIMESTAMP
        };
        this.itemsRef.push(item);
        this.setState({
            currentItem: '',
            username: ''
        });
    }

    componentDidMount() {
        console.log("calling component did mount");

        this.itemsRef.on('value', (snapshot) => {
            let items = snapshot.val();
            let newState = [];
            for (let item in items) {
                newState.unshift({
                    idKey: item,
                    id: items[item].id,
                    title: items[item].title,
                    user: items[item].user,
                    time: items[item].time
                });
            }
            console.log("new state: ", newState);
            this.setState({
                items: newState
            });
        });
    }

    removeItem(itemId) {
        const itemRef = firebase.database().ref(`/items/${itemId}`);
        itemRef.remove();
    }

    render() {
        return (
            <div className='app'>
                <header>
                    <div className="wrapper">
                        <h1>Fun Food Friends</h1>
                        <i className="fa fa-shopping-basket" aria-hidden="true"></i>
                    </div>
                </header>
                <div className='container'>
                    <section className='add-item'>
                        <form onSubmit={this.handleSubmit}>
                            <input type="text" name="username" placeholder="What's your name?"
                                   onChange={this.handleChange} value={this.state.username}/>
                            <input type="text" name="currentItem" placeholder="What are you bringing?"
                                   onChange={this.handleChange} value={this.state.currentItem}/>
                            <button>Add Item</button>
                        </form>
                    </section>
                    <section className='display-item'>
                        <div className="wrapper">
                            <ul>
                                {this.state.items.map((item) => {
                                    return (
                                        <li key={item.id}>
                                            <h3>{item.title}</h3>
                                            <p>brought by: <strong>{item.user}</strong>
                                                <button onClick={() => this.removeItem(item.idKey)}>Remove Item</button>
                                            </p>
                                        </li>
                                    )
                                })}
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
        );
    }
}

export default App;
