import firebase from 'firebase'

const config = {
    apiKey: "AIzaSyAYGZg9-w3tLr6-zKLISv7yj-R6R_4cSdA",
    authDomain: "test001-553f5.firebaseapp.com",
    databaseURL: "https://test001-553f5.firebaseio.com",
    projectId: "test001-553f5",
    storageBucket: "test001-553f5.appspot.com",
    messagingSenderId: "147762767704"
};

firebase.initializeApp(config);

export default firebase;